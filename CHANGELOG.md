# Changelog

## v0.0.0-alpha (2022-10-01)

### Added

- `.scheme` parser 
- pubchem SMILES API retrieval
- mol2chemfig converter system call
- a working basic example