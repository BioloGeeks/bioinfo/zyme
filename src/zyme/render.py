"""Render module for zyme"""
import os
import subprocess
import re
import shutil

from . import search

def debug(string):
    if DEBUG:
        print(string)

DEBUG=os.environ.get("DEBUG", False)

def molecule(molecule):
    debug(f"Rendering {molecule}")
    chemfig = ""
    smiles = search.smiles(molecule)
    if smiles:
        cmd = f"mol2chemfig -zw -i direct \"{smiles}\""
        chemfig = subprocess.check_output(cmd, shell=True, text=True)        
    if not chemfig:
        chemfig = f"\chemfig{{molecule}}"
        chemfig = chemfig.replace('\'', '')
    return chemfig

"""Render a template of chemical schemes into chemfig tex format"""
def render(template):
    template = template.strip().strip('\n')
    # Replace + by \+
    string = template.replace("+", "\+")
    # Replace all arrow <types> by \arrow{<type>}
    arrows = re.findall(r"(<=>|<->|<-|->)", template)
    for arrow in arrows:
        string = string.replace(arrow, r"\arrow{" + arrow + "}")
    string = string.replace(";", "\schemestop\n\n\schemestart")
    # Replace all \t by \quad
    string = string.replace("\t", r"\quad") 
    # Replace all molecule names between curly brackets with their chemfig
    results = re.findall(r"\{.+\}", template)
    allmolecules = re.findall(r'("[^"]{2,}"|[\w\-\,\_]{2,}|[\w]+)', template)
    for match in results:
        mol = match[1:-1]
        if molecule in allmolecules:
            allmolecules.remove(mol)
        string = string.replace(match, molecule(mol))
    # Replace molecules left by \chemfig{molecule}
    for mol in allmolecules:
        string = string.replace(mol, "\chemfig{" + mol + "}").replace('"', '')
    string = string.replace('\n ', '\n').replace('\n    ', '\n').replace('\n\schemestop', '\schemestop')
    with open(os.path.join(os.path.dirname(__file__), "templates/scheme.tex.j2"), "r") as f:
        tpl = f.read()
    return tpl.replace("{ scheme }", string)

def save(schemes, filename, standalone=False):
    debug(f"Saving as {filename} {'in standelone mode' if standalone else ''}")
    tex = ""
    if standalone:
        with open(os.path.join(os.path.dirname(__file__), "templates/standalone.tex.j2"), "r") as f:
            tpl = f.read()
        tex = tpl.replace("{{ schemes }}", schemes)
        shutil.copyfile(os.path.join(os.path.dirname(__file__), "latex/mol2chemfig.sty"), os.path.join(os.path.dirname(filename), "mol2chemfig.sty"))
    else:
        tex = schemes
    with open(filename, "w") as f:
        f.write(tex)