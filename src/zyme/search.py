#! /usr/bin/env python3
import pubchempy as pcp

def smiles(name):
    if name:
        compounds = pcp.get_compounds(name, 'name')
        if compounds:
            return compounds[0].isomeric_smiles
        else:
            print("No compounds found for %s" % name)
            return name
    else:
        ValueError("No name provided")

def main():
    print(smiles('aspirin'))

if __name__ == '__main__':
    main()
