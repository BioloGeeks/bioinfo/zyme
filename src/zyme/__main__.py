#! /usr/bin/env python3
"""
Render a chemical reaction in LaTeX using chemfig

Usage: zyme.py -i <input> -o <output>
"""
import argparse

from . import render

class ToggleAction(argparse.Action):
    def __call__(self, parser, ns, values, option):
        setattr(ns, self.dest, bool("-+".index(option[0])))

parser = argparse.ArgumentParser(
    prog="zyme",
    description="A tool to search for molecules structure from pubchem and render a chemical reactional scheme using chemfig (LaTeX)",
    epilog="Example: zyme -s glycolyse.scheme -o glycolyse.tex",
    prefix_chars="-+"
)

parser.add_argument(
    "-m",
    "--molecule",
    type=str,
    help="Single Molecule to search for and render",
    required=False
)
parser.add_argument(
    "-s",
    "--scheme",
    type=str,
    help="The chemical simplified scheme file to render",
    required=False
)
parser.add_argument(
    "-i",
    "--input",
    type=str,
    help="The chemical simplified scheme as a string",
    required=False
)
parser.add_argument(
    "-o",
    "--output",
    type=str,
    help="The output file to render",
    required=False
)
parser.add_argument(
    "-standalone", 
    "+standalone", 
    action=ToggleAction, 
    nargs=0,
    help="Render a standalone class LaTeX file",
    required=False
)

def main():
    args = parser.parse_args() 
    if args.input:
        with open(args.input, "r") as f:
            scheme = f.read()
    elif args.molecule:
        molecule = args.molecule
        tex = render.molecule(molecule)
    elif args.scheme:
        scheme = args.scheme
        tex = render.render(scheme)
    else:
        raise ValueError("No input provided")
    
    if args.output:
        render.save(tex, args.output, standalone=args.standalone)
    else:
        print(tex)

if __name__ == '__main__':
    main()