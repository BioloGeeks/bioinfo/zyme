# Zyme

Generate enzyme reaction figures using Pubchem's SMILEs and LaTeX/Chemfig.

## Installation

Clone the repository
```bash
git clone git@framagit.org:BioloGeeks/bioinfo/zyme.git
cd zyme
```

Create a Python virtual environment (if you want to).
```bash
python3 -m venv .venv/zyme
source ./.venv/zyme/bin/activate
```

Install zyme
```bash
pip install .
```

This will install the `zyme` command.

## Usage

Call `zyme` with following arguments:

```bash
zyme -i <input.scheme> -o <output.tex>
```

Add `+standalone`, when using `-o` option, if you want get a compilable whole TeX file with chemfig scheme as a standalone document (`documentclass[]{standalone}`).

If not specified, the output file will contains only the reaction scheme (and thus will not be compilable).

To retrieve only the LaTeX/chemfig code, using Pubchem Rest API search, one can use the following command:

```bash
zyme -m limonene
```
(change `limonene` with the name of your molecule of interest as It may be found on Pubchem.)

## References

- [mol2chemfig LaTeX package (legacy)](https://ctan.gutenberg-asso.fr/graphics/mol2chemfig/mol2chemfig-doc.pdf)
- [mol2chemfigPy3 Python package](https://pypi.org/project/mol2chemfigPy3/)
- [chemfig LateX package](https://ctan.org/pkg/chemfig)