
all: 
	@echo "Hello World"

install:
	sudo cp -r ./zyme /home/$(USER)/.local/share/
	echo "export PATH=$$PATH:/home/${USER}/.local/share/zyme" >> ~/.bashrc

uninstall:
	sudo rm -rf /home/$(USER)/.local/share/zyme
	sed -i 's|export PATH=$$PATH:/home/$(USER)/.local/share/zyme||g' ~/.bashrc